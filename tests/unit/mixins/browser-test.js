import Ember from 'ember';
import BrowserMixin from 'kanji-flash-client/mixins/browser';
import { module, test } from 'qunit';

module('Unit | Mixin | browser');

// Replace this with your real tests.
test('it works', function(assert) {
  let BrowserObject = Ember.Object.extend(BrowserMixin);
  let subject = BrowserObject.create();
  assert.ok(subject);
});
