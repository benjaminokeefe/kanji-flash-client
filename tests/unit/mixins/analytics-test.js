import Ember from 'ember';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';
import { module, test } from 'qunit';

module('Unit | Mixin | analytics');

// Replace this with your real tests.
test('it works', function(assert) {
  let AnalyticsObject = Ember.Object.extend(AnalyticsMixin);
  let subject = AnalyticsObject.create();
  assert.ok(subject);
});
