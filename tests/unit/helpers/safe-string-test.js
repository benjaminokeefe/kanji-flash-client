import { safeString } from '../../../helpers/safe-string';
import { module, test } from 'qunit';

module('Unit | Helper | safe string');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = safeString(42);
  assert.ok(result);
});
