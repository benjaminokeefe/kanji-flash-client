// A promise-based mixin for getting browser and location information.
import Ember from 'ember';

export default Ember.Mixin.create({
  browser: Ember.inject.service(),

  getBrowserOperatingSystem () {
    return Promise.resolve(this.get('browser.os'));
  },

  getBrowserClientInfo () {
    return Promise.resolve(this.get('browser.browser'));
  },

  getBrowserLocationInfo () {
    // The call to 'lookup' returns a Promise.
    return this.get('browser').lookup();
  }
});
