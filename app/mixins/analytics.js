import Ember from 'ember';
import BrowserMixin from 'kanji-flash-client/mixins/browser';
import config from 'kanji-flash-client/config/environment';

export default Ember.Mixin.create(BrowserMixin, {
  session: Ember.inject.service(),
  optionalRoutePath: null,

  _mapAnalyticData (os = '', client = {}, location = {}) {
    return {
      userID: this.get('session.currentUser.data.id') || null,
      route: this.get('optionalRoutePath') || `${window.location.pathname}${window.location.search}`,
      dateCreated: new Date(),
      os,
      client,
      location
    };
  },

  _getAnalyticData () {
    const promises = [
      this.getBrowserOperatingSystem(), 
      this.getBrowserClientInfo(), 
      this.getBrowserLocationInfo()
    ];

    return new Promise((resolve, reject) => {
      Promise.all(promises).then(response => {
        const 
          browserOperatingSystem = response[0],
          browserClientInfo = response[1],
          browserLocationInfo = response[2],
          mappedAnalyticData = this._mapAnalyticData(
            browserOperatingSystem, browserClientInfo, browserLocationInfo
          );

        resolve(mappedAnalyticData);        
      }).catch(err => {
        reject(err);
      });
    });
  },

  postAnalyticData (optionalRoutePath) {
    if (optionalRoutePath) {
      this.set('optionalRoutePath', optionalRoutePath);
    }

    return this._getAnalyticData().then(analyticData => {
      Ember.$.ajax({
        type: 'post',
        url:`${config.APP.KANJI_FLASH_API}/analytics`,
        data: JSON.stringify(analyticData),
        contentType: 'application/vnd.api+json',
        dataType: 'json'
      })
    });
  }
});
