import DS from 'ember-data';

export default DS.Model.extend({
  firstName: DS.attr(),
  lastName: DS.attr(),
  email: DS.attr(),
  accountType: DS.attr(),
  isAdmin: DS.attr('boolean'),
  dateCreated: DS.attr('date'),
  receiveNewsletter: DS.attr('boolean'),
  token: DS.attr()
});
