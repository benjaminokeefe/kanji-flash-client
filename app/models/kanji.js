import DS from 'ember-data';

export default DS.Model.extend({
  writing: DS.attr(),
  englishMeanings: DS.attr(),
  frenchMeanings: DS.attr(),
  spanishMeanings: DS.attr(),
  portugeseMeanings: DS.attr(),
  pinyinReadings: DS.attr(),
  koreanRomanReadings: DS.attr(),
  koreanHangulReadings: DS.attr(),
  japaneseOnReadings: DS.attr(),
  japaneseKunReadings: DS.attr(),
  jlptLevel: DS.attr('number'),
  jlptOrder: DS.attr('number'),
  gradeLevel: DS.attr('number')
});