import Ember from 'ember';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';

export default Ember.Route.extend(AnalyticsMixin, {
  afterModel () {
    this.postAnalyticData();
  }
});
