import Ember from 'ember';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';

export default Ember.Route.extend(AnalyticsMixin, {
  model () {
    document.title = 'Kanji Flash - Blog';

    return new Promise ((resolve, reject) => {
      setTimeout(() => {
        this.store.findAll('blog')
        .then(blogs => {
          resolve(blogs);
        })
        .catch(err => {
          reject(err);
        });
      }, 1000);
    });
  },

  afterModel () {
    this.postAnalyticData();
  }
});
