import Ember from 'ember';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';

export default Ember.Route.extend(AnalyticsMixin, {
  queryParams: {
    jlpt: {
      refreshModel: true
    }
  },

  model (params) {
    document.title = `Kanji Flash - JLPT ${params.jlpt.toUpperCase()}`;

    return new Promise ((resolve, reject) => {
      setTimeout(() => {
        this.store.query('kanji', params)
        .then(kanji => {
          resolve(kanji);
        })
        .catch(err => {
          reject(err);
        });
      }, 1000);
    });
    //return new Promise(function (resolve, reject) {});
  },

  afterModel () {
    this.postAnalyticData();
  },

  actions: {
    didTransition () {
      // This is meant to enforce the page moving to an anchor with an ID 
      // that matches the setID in the URL hash, if present.
      // Ember does not currently support this. 4/28/2017
      Ember.$(function () {
        if (this.location.hash) {
          Ember.$('html, body').animate({
            scrollTop: Ember.$(this.location.hash).offset().top
          }, 1000);
        }
      });
    },

    showKanjiDetail (kanji) {
      this.transitionTo('kanji.flashcard', kanji);
    }
  }
});
