import Ember from 'ember';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';

export default Ember.Route.extend(AnalyticsMixin, {
  model (params) {
    return this.store.query('kanji', { literal: params.writing })
    .then(function (kanji) {
      return kanji.get('firstObject');
    });
  },

  afterModel (kanji) {
    document.title = `Kanji Flash - ${kanji.get('writing')}`;
    this.postAnalyticData();
  }
});
