import Ember from 'ember';

export default Ember.Route.extend({
  model () {
    document.title = 'Kanji Flash - Admin - Kanji';

    return new Promise ((resolve, reject) => {
      setTimeout(() => {
        this.store.findAll('kanji')
        .then(kanji => {
          resolve(kanji);
        })
        .catch(err => {
          reject(err);
        });
      }, 1000);
    });
  }
});
