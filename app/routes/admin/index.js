import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service(),

  beforeModel () {
    if (!this.get('session').isAdmin()) {
      this.transitionTo('kanji.index');
      return;
    }

    this.transitionTo('admin.blog');
  }
});
