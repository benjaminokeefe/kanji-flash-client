import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service(),

  beforeModel () {
    if (!this.get('session').isAdmin()) {
      this.transitionTo('kanji.index');
      return;
    }
  },
  
  model () {
    document.title = 'Kanji Flash - Admin - Users';

    return new Promise ((resolve, reject) => {
      setTimeout(() => {
        this.store.findAll('user')
        .then(users => {
          resolve(users);
        })
        .catch(err => {
          reject(err);
        });
      }, 1000);
    });
  }
});