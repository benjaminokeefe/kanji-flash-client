import DS from 'ember-data';
import Ember from 'ember';
import config from 'kanji-flash-client/config/environment';

const inflector = Ember.Inflector.inflector;
inflector.uncountable('kanji');

export default DS.JSONAPIAdapter.extend({
	session: Ember.inject.service(),
	host: config.APP.KANJI_FLASH_API,
	headers: Ember.computed('session.currentUser.data.attributes.token', function () {
		return {
			'x-access-token': this.get('session.currentUser.data.attributes.token') 
		};
	}).volatile()
});
