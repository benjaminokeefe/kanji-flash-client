import Ember from 'ember';
import config from './config/environment';
import googlePageview from './mixins/google-pageview';

const Router = Ember.Router.extend(googlePageview, {
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('kanji', {}, function() {
    this.route('flashcard', { path: '/:writing' });
  });
  this.route('blogs', function() {
    this.route('blog', { path: '/:blog_id' });
  });
  this.route('admin', {}, function() {
    this.route('blog', {}, function() {
      this.route('new', {});
    });
    this.route('user', {}, function() {
      this.route('new', {});
    });
    this.route('kanji', {}, function() {});
  });
  this.route('login', {}, function() {
    this.route('create');
  });
});

export default Router;
