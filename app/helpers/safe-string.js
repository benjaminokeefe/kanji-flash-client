import Ember from 'ember';

export function safeString(html) {
  return Ember.String.htmlSafe(html);
}

export default Ember.Helper.helper(safeString);
