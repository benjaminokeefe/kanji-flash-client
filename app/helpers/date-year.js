import Ember from 'ember';

export function dateYear () {
  return new Date().getFullYear();
}

export default Ember.Helper.helper(dateYear);
