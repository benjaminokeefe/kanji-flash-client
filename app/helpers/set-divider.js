import Ember from 'ember';

export function setDivider(params) {
  const 
    index = params[0],
    setCount = params[1],
    setNumber = getSetNumber(index, setCount),
    url = getUrl(`#set${setNumber}`);

  if (index % setCount === 0 || index === 0) {
    const html = `
      <div class="set-divider">
        <a id="set${setNumber}" href="${url}">
          Set ${setNumber}
          <span class="link-label">Click to link to set ${setNumber}</span>
        </a>
      </div>
    `;

    return Ember.String.htmlSafe(html);
  } else {
    return;
  }
}

function getSetNumber (index, setCount) {
  if (index === 0) {
    return 1;
  } else {
    return (index / setCount) + 1;
  }
}

function getUrl (urlHash) {
  const 
    url = window.location.href,
    urlWithoutHash = url.replace(window.location.hash, ''),
    urlWithHash = urlWithoutHash.concat(urlHash);

  return urlWithHash;
}

export default Ember.Helper.helper(setDivider);
