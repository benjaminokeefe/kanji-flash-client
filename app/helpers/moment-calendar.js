import Ember from 'ember';
import moment from 'moment';

export function momentCalendar(params) {
  return moment(params[0]).calendar();
}

export default Ember.Helper.helper(momentCalendar);
