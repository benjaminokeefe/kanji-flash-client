import Ember from 'ember';
import moment from 'moment';

export function propertyValue(params) {
  if (params[1].includes('date')) {
    return moment(params[0].get(params[1])).calendar();
  } else {
    return params[0].get(params[1]);
  }
}

export default Ember.Helper.helper(propertyValue);
