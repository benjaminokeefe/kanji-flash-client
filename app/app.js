import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

let App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

loadInitializers(App, config.modulePrefix);

export default App;

// Handle keyboard events across the app
(function () {
  document.addEventListener('keydown', function (e) {
    const key = e.keyCode;

    switch (key) {
      case 27: { // esc key
        const closeButton = document.getElementsByClassName('close');
        if (closeButton.length) {
          closeButton[0].click();
        }
        break;
      }
      case 37: { // left arrow
        const leftArrow = document.getElementsByClassName('last ember-view arrow-button-component');
        if (leftArrow.length) {
          leftArrow[0].click();
        }
        break;
      }
      case 39: { // right arrow
        const rightArrow = document.getElementsByClassName('next ember-view arrow-button-component');
        if (rightArrow.length) {
          rightArrow[0].click(); 
        }
        break;
      }
      case 13: { // enter key
        const flashcard = document.getElementsByClassName('flash-card-component');
        if (flashcard.length) {
          flashcard[0].click(); 
        }
        break;
      }
      default:
         return;
    }
  });
}());

