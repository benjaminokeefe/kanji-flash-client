import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'table-data-component',
  tagName: 'table',

  actions: {
    delete (item) {
      this.sendAction('action', item);
    }
  }
});
