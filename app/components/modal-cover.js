import Ember from 'ember';

export default Ember.Component.extend({
	classNames: 'modal-cover-component',

	click (e) {
		this.sendAction('action', e);
	}
});
