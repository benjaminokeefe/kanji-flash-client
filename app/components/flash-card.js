import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'flash-card-component',
  classNameBindings: 'front:front:back',
  initialClientXPosition: null,
  distanceToSwipe: 75,
  isNewCard: false,

  click () {
    this.sendAction();
  },

  touchStart (e) {
    this.set('isNewCard', false);
    this.set('initialClientXPosition', e.touches[0].clientX);
  },

  touchMove (e) {
    if (this.get('isNewCard')) return;

    const 
      initialClientXPosition = this.get('initialClientXPosition'),
      clientX = e.touches[0].clientX,
      distanceToSwipe = this.get('distanceToSwipe');

    if (clientX > (initialClientXPosition + distanceToSwipe)) {
      this.sendAction('last');
      this.set('isNewCard', true);
    } else if (clientX < (initialClientXPosition - distanceToSwipe)) {
      this.sendAction('next');
      this.set('isNewCard', true);
    }
  }
});
