import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'general-button-component',
  tagName: 'button',

  click () {
    this.sendAction();
  }
});