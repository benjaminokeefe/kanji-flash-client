import Ember from 'ember';

export default Ember.Component.extend({
	classNames: 'list-item-component',

	click () {
		this.sendAction('action', this.get('item'));
	},

	displayText: Ember.computed('item', 'displayProperty', function () {
		return this.get(`item.${this.get('displayProperty')}`);
	})
});
