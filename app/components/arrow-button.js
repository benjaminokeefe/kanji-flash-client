import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'arrow-button-component',

  click () {
    this.sendAction();
  }
});
