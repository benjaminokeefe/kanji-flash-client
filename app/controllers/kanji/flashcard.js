import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: 'front',
  front: true,
  kanjiController: Ember.inject.controller('kanji.index'),
  kanji: Ember.computed.reads('kanjiController.model'),
  language: 'english',

  actions: {
    flip () {
      this.toggleProperty('front');
    },

    last () {
      if (this.get('isFirstKanji')) { return; }

      const
        kanji = this.get('kanji'),
        currentIndex = kanji.indexOf(this.get('model')),
        lastKanji = kanji.objectAt(currentIndex - 1);

        this.transitionToRoute('kanji.flashcard', lastKanji, { queryParams: { 'front': true }});
    },

    next () {
      if (this.get('isLastKanji')) { return; }

      const
        kanji = this.get('kanji'),
        currentIndex = kanji.indexOf(this.get('model')),
        nextKanji = kanji.objectAt(currentIndex + 1);

        this.transitionToRoute('kanji.flashcard', nextKanji, { queryParams: { 'front': true }});
    },

    modalClicked (e) {
      if (e.target.className.includes('modal-cover-component') || e.target.className.includes('close')) {
        this.transitionToRoute('kanji', { queryParams: { "jlpt": this.get('model.jlptLevel') }});
      }
    }
  },

  isFirstKanji: Ember.computed('model', 'kanji', function () {
    const kanji = this.get('kanji');

    if (!kanji) {
      return true;
    } else {
      return kanji.indexOf(this.get('model')) > 0 ? false : true;
    }
  }),

  isLastKanji: Ember.computed('model', 'kanji', function () {
    const kanji = this.get('kanji');

    if (!kanji) {
      return true;
    } else {
      return kanji.indexOf(this.get('model')) < (kanji.get('length') - 1) ? false : true;
    }
  }),

  isSingleKanji: Ember.computed('model', 'kanji', function () {
    const kanji = this.get('kanji');
    return kanji ? false : true;
  }),

  meanings: Ember.computed('model', 'kanji', function () {
    return this.model.get(`${this.get('language')}Meanings`).join(', ');
  }),

  onyomis: Ember.computed('model', 'kanji', function () {
    return this.model.get('japaneseOnReadings').join(', ');
  }),

  kunyomis: Ember.computed('model', 'kanji', function () {
    return this.model.get('japaneseKunReadings').join(', ');
  })
});
