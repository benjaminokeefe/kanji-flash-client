import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service(),
  showDonationBanner: true,
  
  actions: {
    logout () {
      this.get('session').logout();
      this.transitionToRoute('login');
    }
  },
  
  isAdmin: Ember.computed('session.currentUser', function () {
    return this.get('session').isAdmin();
  })
});
