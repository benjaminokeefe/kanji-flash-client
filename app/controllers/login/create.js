import Ember from 'ember';

export default Ember.Controller.extend({
  receiveNewsletter: false,
  validation: Ember.inject.service(),
  session: Ember.inject.service(),

  actions: {
    toggleNewsletter () {
      this.toggleProperty('receiveNewsletter');
    },

    createUser () {
      const
        email = this.get('email'),
        password = this.get('password'),
        validation = this.get('validation');
      
      if (!validation.isValidEmail(email)) {
        this.set('errorMessage', 'Please enter a valid email address');
      } else if (!validation.isValidPassword(password)) {
        this.set('errorMessage', 'Please enter a password');
      } else {
        validation.isUniqueEmail(email)
        .then(unique => {
          if (unique) {
            this.set('errorMessage', '');
            
            let user = this.store.createRecord('user', {
              email: email,
              password: password
            });
            
            user.save()
            .then(() => {
              return this.get('session').login(email, password);
            })
            .then(() => {
              // TODO: REDIRECT TO THANK YOU PAGE, WHICH WILL CONTAIN A LINK TO LOGIN
            })
            .catch(() => {
              user.deleteRecord();
            });
          } else {
            this.set('errorMessage', 'Sorry, that email is already in use');
          }
        })
        .catch(() => {
          this.set('errorMessage', 'A login error has occurred');
        });
      }
    }
  }
});
