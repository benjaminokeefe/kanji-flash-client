import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service(),
  validation: Ember.inject.service(),
  
  actions: {
    authenticate () {
      const 
        email = this.get('email'), 
        password = this.get('password'),
        validation = this.get('validation');
      
      if (!validation.isValidEmail(email)) {
        this.set('errorMessage', 'Please enter a valid email address');
      } else if (!validation.isValidPassword(password)) {
        this.set('errorMessage', 'Please enter a password');
      } else {
        this.set('errorMessage', '');
        this.get('session').login(email, password)
        .then(() => {
          this.transitionToRoute('kanji', { queryParams: { "jlpt": 'n5' }});
        })
        .catch(() => {
          this.set('errorMessage', 'Invalid username or password');
        });
      }
    }
  }
});
