import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['dateCreated:desc'],
  blogs: Ember.computed.sort('model', 'sortProperties')
});
