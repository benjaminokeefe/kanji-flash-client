import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['lastName:asc'],
  users: Ember.computed.sort('model', 'sortProperties'),
  headers: ['first name', 'last name', 'email', 'created'],
  propsToDisplay: ['firstName', 'lastName', 'email', 'dateCreated'],

  actions: {
    createUser () {
      this.transitionToRoute('admin.user.new');
    },

    deleteUser (user) {
      if (confirm(`Delete ${user.get('firstName')} ${user.get('lastName')}?`)) {
        user.destroyRecord();
      } else {
        return;
      }
    }
  }
});