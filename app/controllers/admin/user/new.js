import Ember from 'ember';

export default Ember.Controller.extend({
  isInvalid: false,

  actions: {
    createUser () {
      if (!this.isValid()) {
        this.set('isInvalid', true);
        return;
      } else {
        this.set('isInvalid', false);
      }

      let user = this.store.createRecord('user', {
        firstName: this.get('firstName'),
        lastName: this.get('lastName'),
        email: this.get('email'),
        dateCreated: new Date()
      });

      user.save()
      .then(() => {
        this.set('firstName', '');
        this.set('lastName', '');
        this.set('email', '');
        this.transitionToRoute('admin.user.index');
      });
    }
  },

  isValid () {
    return this.get('firstName') && this.get('lastName') && this.get('email');
  }
});
