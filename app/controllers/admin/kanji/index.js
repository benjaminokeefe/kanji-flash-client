import Ember from 'ember';
import config from 'kanji-flash-client/config/environment';

export default Ember.Controller.extend({
  sortProperties: ['jlpt:desc'],
  kanji: Ember.computed.sort('model', 'sortProperties'),
  headers: ['writing', 'meanings', 'onyomis', 'kunyomis', 'jlpt'],

  actions: {
    insertKanji () {
      Ember.$.post(`${config.APP.KANJI_FLASH_API}/kanji`)
      .done(() => {
        this.store.findAll('kanji');
      });
    },

    deleteKanji () {
      Ember.$.ajax({ url: `${config.APP.KANJI_FLASH_API}/kanji`, type: 'DELETE' })
      .done(() => {
        this.store.unloadAll('kanji');
      });
    }
  }
});
