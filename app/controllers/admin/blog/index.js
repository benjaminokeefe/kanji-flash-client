import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['dateCreated:desc'],
  blogs: Ember.computed.sort('model', 'sortProperties'),
  headers: ['created', 'title', 'author'],
  propsToDisplay: ['dateCreated', 'title', 'author'],

  actions: {
    createBlog () {
      this.transitionToRoute('admin.blog.new');
    },

    deleteBlog (blog) {
      if (confirm(`Delete ${blog.get('title')}?`)) {
        blog.destroyRecord();
      } else {
        return;
      }
    }
  }
});
