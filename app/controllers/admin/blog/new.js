import Ember from 'ember';

export default Ember.Controller.extend({
  isInvalid: false,

  actions: {
    createBlog () {
      if (!this.isValid()) {
        this.set('isInvalid', true);
        return;
      } else {
        this.set('isInvalid', false);
      }

      let blog = this.store.createRecord('blog', {
        dateCreated: new Date(),
        title: this.get('title'),
        author: 'Ben O\'Keefe',
        entry: this.get('entry')
      });

      blog.save()
      .then(() => {
        this.set('title', '');
        this.set('entry', '');
        this.transitionToRoute('admin.blog.index');
      });
    }
  },

  isValid () {
    return this.get('title') && this.get('entry');
  }
});
