import Ember from 'ember';
import config from 'kanji-flash-client/config/environment';
import AnalyticsMixin from 'kanji-flash-client/mixins/analytics';

export default Ember.Service.extend(AnalyticsMixin, {
  currentUser: null,
  store: Ember.inject.service(),
  
  login (email, password) {
    return new Promise((resolve, reject) => {
      Ember.$.ajax({
          type: 'POST',
          url: `${config.APP.KANJI_FLASH_API}/login`,
          contentType: 'application/vnd.api+json',
          data: JSON.stringify({ username: email, password: password }),
          processData: false,
          success: function (data) {
            resolve(data);
            this.get('store').push(data);
            this.set('currentUser', data);
            Cookies.set('user', data);

            this.postAnalyticData('/login');
          }.bind(this),
          error: function () {
            reject('Invalid username or password');
          }.bind(this)
      });
    });
  },
  
  logout () {
    this.postAnalyticData('/logout').then(this.nullifyUser.bind(this), this.nullifyUser.bind(this));    
  },
  
  initializeFromCookie: function () {
    const userCookie = Cookies.get('user');
    
    if (userCookie) {
        const parsedCookie = JSON.parse(userCookie);
        this.get('store').push(parsedCookie);
        this.set('currentUser', parsedCookie);
    }
  }.on('init'),
  
  isAdmin () {
    const currentUser = this.get('currentUser');
    return currentUser ? currentUser.data.attributes.isAdmin : false; 
  },

  nullifyUser () {
    this.set('currentUser', null);
    Cookies.remove('user');
  },
  
  mapUserData (data) {
    const attr = data.data.attributes;
    
    return {
      id: data.data.id,
      accountType: attr.accountType,
      dateCreated: attr.dateCreated,
      email: attr.email,
      firstName: attr.firstName,
      lastName: attr.lastName,
      isAdmin: attr.isAdmin,
      receiveNewsletter: attr.receiveNewsletter,
      token: attr.token,
    };
  }
});
