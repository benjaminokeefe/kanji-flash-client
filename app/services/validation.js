import Ember from 'ember';
import config from 'kanji-flash-client/config/environment';

export default Ember.Service.extend({
  isValidEmail (email) {
    const regex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    return regex.test(email);
  },
  
  isValidPassword (password) {
    return Boolean(password);
  },
  
  isUniqueEmail (email) {
    return new Promise((resolve, reject) => {
      Ember.$.get(`${config.APP.KANJI_FLASH_API}/users/validate/${email}?property=email`)
      .done(data => {
        resolve(!data);
      })
      .fail(err => {
        reject(err);
      });
    });
  } 
});
